#! /usr/bin/ruby
# Copyright (c) 2007, Stuart Glaser <StuGlaser@gmail.com>
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
# 
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

require 'net/http'
require 'json'

# The JSON parser stupidly does not accept just strings.
def hacked_json_parse(json)
  boxed = "[ " + json + " ]"
  boxed_parsed = JSON.parse boxed
  boxed_parsed[0]
end

class CouchException < StandardError
  attr_reader :couch, :response

  def initialize(couch, response)
    @couch = couch
    @response = response
  end

  def couch_backtrace
    #json = JSON.parse @response.body
    json = hacked_json_parse @response.body
    json['error']['id'] + "\n" + json['error']['reason']
  end

  def to_s
    "#{@response.class}\n" +
      "Couch Backtrace:" + couch_backtrace +
      "\nRuby Backtrace:"
  end
end

module Couch
  
  class Server
    attr_accessor :host, :port

    def initialize(host, port)
      @host = host
      @port = port
    end
    
    def get(path)
      request Net::HTTP::Get.new(path)
    end

    def put(path, data)  # TODO: support text/javascript
      req = Net::HTTP::Put.new(path)
      req['content-type'] = 'text/javascript'
      req['content-type'] = 'application/json'
      req.body = data
      request req
    end

    def post(path, data)  # TODO: support text/javascript
      req = Net::HTTP::Post.new(path)
      req.body = data
      request req
    end

    def delete(path)
      request Net::HTTP::Delete.new(path)
    end

    def request(req)
      resp = Net::HTTP.start(@host, @port) do |http|
        http.request req
      end
      if not resp.kind_of?(Net::HTTPSuccess)
        raise CouchException.new(self, resp)
      end
      resp
    end

  end


  class Db
    attr_accessor :server
    attr_accessor :name

    def initialize(server, name)
      @server = server
      @name = name
    end

    def get(doc_id)
      resp = @server.get doc_path(doc_id)
      #JSON.parse resp.body
      hacked_json_parse resp.body
    end

    def put(doc_id, body)
      resp = $server.put doc_path(doc_id), body.to_json
      #JSON.parse resp.body
      hacked_json_parse resp.body
    end

    def post(body)
      resp = $server.post doc_path(''), body.to_json
      #JSON.parse resp.body
      hacked_json_parse resp.body
    end

    def delete(doc_id)
      resp = $server.delete doc_path(doc_id)
      #JSON.parse resp.body
      hacked_json_parse resp.body
    end

    def temp_view(data)
      path = doc_path('_temp_view')
      req = Net::HTTP::Post.new(path)
      req['content-type'] = 'text/javascript'
      req.body = data
      request req
    end
    
    def temp_view(body)  # TODO: content-type
      resp = $server.post doc_path('_temp_view'), body
      #JSON.parse resp.body
      hacked_json_parse resp.body
    end

    def doc_path(doc_id)
      '/' + @name + '/' + doc_id
    end
  end


  # Represents a document in a Couch database
  class Doc
    attr_reader :db
    attr_accessor :id
    attr_reader :_rev
    attr_accessor :_attachments

    def _attachments
      @_attachments = [] if @_attachments.nil?
      @_attachments
    end

    class << self
      attr_reader :field_names
      
      def field(*syms)
        attr_accessor(*syms)
        @field_names = [] if @field_names.nil?
        @field_names |= syms
        nil
      end
    end

    def Doc.find(db, id)
      begin
        doc = new $db
        doc.from_couch! $db.get(id)
        doc
      rescue CouchException=>ex
        return nil if ex.response.instance_of? Net::HTTPNotFound
        raise
      end
    end
    

    def Doc.find_or_create(db, id)
      doc = find db, id
      return doc unless doc.nil?

      doc = new db
      doc.id = id
      doc.save
      doc
    end

    
    def initialize(db)
      @db = db
    end

    
    def save
      print "SAVE #{id}\n"
      fields = to_couch
      results = if @id.nil?  # POST
                  @db.post fields
                else         # PUT
                  @db.put @id, fields
                end
      @_rev = results['rev']
      @id = results['id']
      results
    end

    def refresh!
      raise "Never saved" if @id.nil?
      self.from_couch! @db.get(@id)
      nil
    end

    def delete!
      results = @db.delete(@id + "?rev=#{@_rev}")
      @_rev = nil
      results
    end

    def new_attachment
      a = Attachment.new self
      _attachments << a
      a
    end

    # Converts the object to (unparsed) JSON format for couch
    def to_couch
      # Fills in the fields for saving to the db
      fields = {}
      fields['_rev'] = @_rev unless @_rev.nil?
      self.class.field_names.each do |field|
        fields[field] = instance_variable_get '@' + field.to_s
      end
      unless _attachments.empty?
        # Let ye of functional natures rejoice!
        fields['_attachments'] = _attachments.inject({}) do |all, a|
          all.merge a.to_couch
        end
      end
      fields
    end

    # Takes (parsed) JSON from couch and places it into the fields
    def from_couch!(couch)
      @id = couch['_id']
      @_rev = couch['_rev']
      self.class.field_names.each do |field|
        instance_variable_set('@' + field.to_s, couch[field.to_s])
      end
      unless couch['_attachments'].nil?
        @_attachments = []
        couch['_attachments'].each do |name,value|
          a = Attachment.new self
          a.from_couch! name, value
          @_attachments << a
        end
      end
      nil
    end
  end

  
  class Attachment
    attr_accessor :doc
    attr_accessor :name, :type, :data
    attr_reader :length, :stub

    def data=(data)
      @stub = data.nil?
      @data = data
    end

    def data
      if not @data.nil?
        @data
      else
        path = @doc.id + '?attachment=' + @name
        @doc.db.get path
      end
    end

    def initialize(doc)
      @doc = doc
      @stub = true
    end

    def to_couch
      a = { 'type' => type, 'stub' => stub }
      a['data'] = data unless stub
      { name => a }
    end

    def from_couch!(name, couch)
      @name = name
      @stub = couch['stub'] or false
      @type = couch['type']
      @length = couch['length']
      @data = nil
    end
  end
  
end


true
