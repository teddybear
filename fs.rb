#! /usr/bin/ruby
# Copyright (c) 2007, Stuart Glaser <StuGlaser@gmail.com>
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
# 
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

# To mount the teddybear filesystem, run:
#
#   $ fs.rb some/place/to/mount/at

require 'fusefs'
require 'couch'

$server = Couch::Server.new 'localhost', 8888
$db = Couch::Db.new $server, 'teddybear'

def print_err(err)
  print err.message + "\n"
  print err.backtrace.join("\n")
end  

class String
  def lchomp(separator = $/)
    reverse.chomp(separator).reverse
  end
end


def peel_path(path)
  return nil if path.nil?
  path.lchomp('/').split('/', 2)
end


class CouchDir < Couch::Doc
  field :subdirs, :files

  def subdirs
    @subdirs = [] if @subdirs.nil?
    @subdirs
  end
  def files
    @files = [] if @files.nil?
    @files
  end

  def contents(path)
    print "CONTENTS #{path}\n"
    first, rest = peel_path path
    if first.nil?
      subdirs + files
    else
      find_subdir(first).contents rest
    end
  end
  
  def file?(path)
    first, rest = peel_path path
    if rest.nil?
      files.include? first
    else
      find_subdir(first).file? rest
    end
  end

  def directory?(path)
    first, rest = peel_path path
    return true if first.nil?

    if rest.nil?
      subdirs.include? first
    else
      find_subdir(first).directory? rest
    end
  end

  def read_file(path)
    first, rest = peel_path path
    if rest.nil?
      raise "shit" unless files.include? first
      find_file(first).data
    else
      find_subdir(first).read_file rest
    end
  end

  def write_to(path, data)
    first, rest = peel_path path
    if rest.nil?
      if not files.include? first
        files << first
        save
      end
      # Updates the file in couch
      f = find_file first
      f.data = data
      f.save
    else
      find_subdir(first).write_to rest, data
    end
  end

  def delete_file(path)
    first, rest = peel_path path
    if rest.nil?
      find_file(first).delete_file
      files.delete first
      save
    else
      find_subdir(first).delete_file rest
    end
  end

  def mkdir(path)
    first, rest = peel_path path
    if rest.nil?
      subdirs << first
      save
    else
      find_subdir(first).mkdir rest
    end
  end

  def rmdir(path)
    print "RMDIR #{id}: #{path}\n"
    first, rest = peel_path path
    if first.nil?
      delete!
    else
      if rest.nil?
        subdirs.delete first
        save
      end
      find_subdir(first).rmdir rest
    end
  end

  def size(path)
    first, rest = peel_path path
    if rest.nil?
      find_file(first).size
    else
      find_subdir(first).size rest
    end
  end

  # Helper for grabbing the couch record for a subdirectory.
  def find_subdir(dir)
    subdir_id = id + '/' + dir
    d = CouchDir.find_or_create @db, subdir_id
    d.save if d._rev.nil?
    d
  end

  # Helper for grabbing the couch record for a file.
  def find_file(file)
    file_id = id + '/' + file
    f = CouchFile.find_or_create @db, file_id
    f.save if f._rev.nil?
    f
  end

end


class CouchFile < Couch::Doc
  field :data

  def delete_file
    delete!
  end

  def size
    data.size
  end
end


class Root
  attr_accessor :name

  # Grabs the root directory
  def root
    CouchDir.find_or_create @db, @name
  end

  def initialize(db, name)
    @db = db
    @name = name
  end

  def contents(path)
    begin
      print "contents #{path}\n"
      root.contents path.lchomp('/')  # num num num
    rescue =>err
      print_err err
    end
  end

  def directory?(path)
    print "directory? #{path}\n"
    root.directory? path.lchomp('/')
  end

  def file?(path)
    print "file? #{path}\n"
    root.file? path.lchomp('/')
  end

  def mkdir(path)
    begin
      print "mkdir #{path}\n"
      root.mkdir path.lchomp('/')
    rescue =>err
      print_err err
    end
  end

  def rmdir(path)
    begin
      print "rmdir #{path}\n"
      root.rmdir path.lchomp('/')
    rescue =>err
      print_err err
    end
  end

  def read_file(path)
    begin
      print "read_file #{path}\n"
      root.read_file path.lchomp('/')
    rescue => err
      print_err err
    end
  end

  def write_to(path, data)
    begin
      print "write_to #{path}, #{data.size} bytes\n"
      root.write_to path.lchomp('/'), data
    rescue =>err
      print_err err
    end
  end

  def delete(path)
    begin
      print "delete #{path}\n"
      root.delete_file path.lchomp('/')
    rescue =>err
      print_err err
    end
  end

  def size(path)
    root.size path.lchomp('/')
  end

  def can_mkdir?(path)
    true
  end

  def can_rmdir?(path)
    true
  end

  def can_write?(path)
    print "can_write? #{path}\n"
    true
  end

  def can_delete?(path)
    true
  end
end


mount_point = ARGV.shift

root = Root.new $db, mount_point
FuseFS.set_root root

FuseFS.mount_under mount_point
begin
  FuseFS.run
ensure
  FuseFS.unmount
end

